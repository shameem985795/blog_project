@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="header-title text-center mb-3">রিপোর্ট ডিটেইল</h4>
                    <div class="p-2">

                        {{-- @include('elements.message')
                        @include('elements.error') --}}

                        <form action="{{ route('rtb.update', $reportTemplate->id) }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" name="title_bn" value="{{ $reportTemplate->title_bn }}" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="description" rows="5">{{ $reportTemplate->description }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">{{ __('ইজ অ্যাক্টিভ?') }}</label>
                                <div class="col-sm-1">
                                    <input type="checkbox" name="is_active" class="form-control" {{ $reportTemplate->is_active ? 'checked' : '' }}>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container -->
@endsection

@push('js')
    <!-- <script>
        const elements = document.getElementsByClassName("jsonFormat");

        Array.from(elements).forEach(element => {
            element.addEventListener('input', e => {
                e.target.value = JSON.stringify(e.target.value);
                e.target.value = e.target.value.replace(/\\n/g, '');
            });
        });
    </script> -->
@endpush
