@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="header-title text-center mb-3">{{ __('Report List') }}</h4>
                    <div class="row">
                        <div class="col-md-9 offset-md-1 col-sm-12 offset-sm-0">
                            <div class="text-right">
                            <a href="{{route('rtb.create')}}" class="btn btn-info mb-3">{{ __('Create') }}</a>
                            </div>
                            <div class="table-responsive">

                                {{-- @include('elements.message')
                                @include('elements.error') --}}

                                <table class="table mb-0">
                                    <thead>
                                        <tr>
                                            <th>{{ __('Title') }}</th>
                                            <th style="text: center">পদক্ষেপ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach ($reportTemplates as $reportTemplate)


                                            <tr>
                                            <td>{{$reportTemplate->title_bn}}</td>
                                                <td>
                                                <a href="{{route('rtb.view',$reportTemplate->id)}}">View</a> |
                                                <a href="{{route('rtb.edit',$reportTemplate->id)}}">Edit</a> |
                                                    <a href="{{ route('rtb.standard-editor', $reportTemplate->id) }}">Standard Editor</a> |
                                                    {{--  <a href="{{ route('rtb.advanced-editor', $reportTemplate->id) }}">Advanced Editor</a> |
                                                    <a href="{{ route('rtb.handle-migration-generation', $reportTemplate->id) }}">Generate Migration</a> |
                                                    <a href="{{ route('rtb.run-migration', $reportTemplate->id) }}">Run Migration</a> |
                                                    <a href="{{ route('rtb.handle-form-view', $reportTemplate->id) }}">View Form </a> |
                                                    <a href="{{ route('rtb.handle-view-report', $reportTemplate->id) }}">View Report </a> |
                                                    <a href="#">Close</a> |
                                                    <!-- <a href="{{ route('rtb.destroy', $reportTemplate->id) }}">Delete</a> | -->
                                                    <a href="#" id="configure">Configure</a>
                                                    <a href="{{ route('report_template_configuration', $reportTemplate->id) }}">Configure2</a> |
                                                    @if(!$reportTemplate->applied_at)
                                                    {!! Form::open([
                                                            'route' => ['apply_report_template', $reportTemplate->id],
                                                            'style' => 'display: inline'
                                                    ]) !!}

                                                    <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure want to apply the template ? ')">Apply</button>

                                                    {!! Form::close() !!}
                                                    @endif --}}
                                                </td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                                {{-- {{ $reportTemplates->links() }} --}}
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
        <!--- end row -->
    </div> <!-- container -->
{{--
    <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle"
            aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="messages"><ul></ul></div>
                    <form id="logForm" method="post">
                        <input type="hidden" name="id" id="hiddenId">
                        <input type="hidden" name="data_model" id="dataModel" value="Report" class="form-control mt-0">
                        <h4 class="text-center mt-3"> রিপোর্ট পারমিশন </h4>
                        <div class="container mt2">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h6><mark>তথ্য সংগ্রহ </mark> </h6>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input  class="form-check-input" name="totho_nij_office" id="totho_nij_office" value="1" type="checkbox"/>
                                                    <label class="form-check-label"  for="totho_nij_office">
                                                        নিজ অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="totho_urdhoton_office" id="totho_urdhoton_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="totho_urdhoton_office">
                                                        ঊর্ধ্বতন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="totho_sokol_urdhoton_office"  id="totho_sokol_urdhoton_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="totho_sokol_urdhoton_office">
                                                        সকল ঊর্ধ্বতন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="totho_odhoston_office" id="totho_odhoston_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="totho_odhoston_office">
                                                        অধঃস্তন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="totho_sokol_odhoston_office" id="totho_sokol_odhoston_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="totho_sokol_odhoston_office">
                                                        সকল অধঃস্তন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h6><mark>অন্য মন্ত্রণালয়  </mark> </h6>

                                            <select class="form-control" name="office_ministry_id" id="office_ministry">
                                                <option class="form-control"> select  </option>
                                            </select>
                                            <h6 class="mt-2"><mark>  অফিস   </mark> </h6>
                                            <select class="form-control" id="office" name="office_id"class="mt-2">
                                            </select>
                                            <!--                                                    <div class="form-group">-->
                                            <!--                                                        <div class="form-check">-->
                                            <!--                                                            <input class="form-check-input" name="totho_montranaloy"  id="totho_montranaloy" value="1" type="checkbox" >-->
                                            <!--                                                            <label class="form-check-label" for="totho_montranaloy">-->
                                            <!--                                                                তথ্য  মন্ত্রণালয়-->
                                            <!--                                                            </label>-->
                                            <!--                                                        </div>-->
                                            <!--                                                    </div>-->
                                            <!--                                                    <div class="form-group">-->
                                            <!--                                                        <div class="form-check">-->
                                            <!--                                                            <input class="form-check-input" name="pani_sompad_totho_montranaloy"  id="pani_sompad_totho_montranaloy" value="1" type="checkbox">-->
                                            <!--                                                            <label class="form-check-label" for="pani_sompad_totho_montranaloy">-->
                                            <!--                                                                পানি-সম্পদ-মন্ত্রণালয়-->
                                            <!--                                                            </label>-->
                                            <!--                                                        </div>-->
                                            <!--                                                    </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h6><mark>রিপোর্ট দেখবে </mark> </h6>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="report_nij_office" id="report_nij_office" value="1" type="checkbox"/>
                                                    <label class="form-check-label" for="report_nij_office">
                                                        নিজ অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="report_urdhoton_office" id="report_urdhoton_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="report_urdhoton_office">
                                                        ঊর্ধ্বতন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="report_sokol_urdhoton_office" id="report_sokol_urdhoton_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="report_sokol_urdhoton_office">
                                                        সকল ঊর্ধ্বতন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="report_odhoston_office" id="report_odhoston_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="report_odhoston_office">
                                                        অধঃস্তন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="report_sokol_odhoston_office" id="report_sokol_odhoston_office" value="1" type="checkbox" id="">
                                                    <label class="form-check-label" for="report_sokol_odhoston_office">
                                                        সকল অধঃস্তন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h6><mark>সারসংক্ষেপ </mark> </h6>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="sarmmary_nij_office" id="sarmmary_nij_office" value="1" type="checkbox"/>
                                                    <label class="form-check-label" for="sarmmary_nij_office">
                                                        নিজ অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="sarmmary_urdhoton_office" id="sarmmary_urdhoton_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="sarmmary_urdhoton_office">
                                                        ঊর্ধ্বতন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="sarmmary_sokol_urdhoton_office" id="sarmmary_sokol_urdhoton_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="sarmmary_sokol_urdhoton_office">
                                                        সকল ঊর্ধ্বতন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="sarmmary_odhoston_office"  id="sarmmary_odhoston_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="sarmmary_odhoston_office">
                                                        অধঃস্তন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <input class="form-check-input" name="sarmmary_sokol_odhoston_office" id="sarmmary_sokol_odhoston_office" value="1" type="checkbox">
                                                    <label class="form-check-label" for="sarmmary_sokol_odhoston_office">
                                                        সকল অধঃস্তন অফিস
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close" class="btn btn-round btn-danger font-12" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" id="create" onclick="create()" class="btn btn-round btn-info font-12">Create</button>
                    <button type="button" id="update" onclick="update()" class="btn btn-round btn-info font-12">Update</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
{{--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>--}}
    {{-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> --}}
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#configure').click(function () {
                $('#modalForm').modal('show')
                getMisnistry()
            });
        });
        function getMisnistry() {
            axios.get('http://dorms-apiengine.local/getOfficeMinistry')
                .then(response=>{
                    response.data.forEach(ministryOption)
                })
                .catch(function (error) {
                    console.log(error)
                })
        }

        $('#office_ministry').on('change', function () {
            getOffice()
        })

        function ministryOption(data) {
            document.getElementById('office_ministry').innerHTML += `<option  value="${data.id}">${data.name_bng}</option>`;
        }


        function getOffice() {
            let id = document.getElementById('office_ministry').value;
            axios.get('http://dorms-apiengine.local/getOffice/' + id )
                .then(response=>{
                    console.log(response)
                    document.getElementById('office').innerHTML = " ";
                    response.data.forEach(officeOption)
                })
                .catch(function (error) {
                    console.log(error)
                })
        }

        function officeOption(data) {
            document.getElementById('office').innerHTML += `<option  value="${data.id}">${data.office_name_bng}</option>`;
        }

        function create() {
            let data = $('#logForm').serialize();

            axios.post('http://dorms-apiengine.local/datamodel/store', data)
                .then(response => {
                    $('#close').trigger('click');
                    swal({
                        title: "Good job!",
                        text: "Successfully Created!",
                        icon: "success",
                        button: "Close",
                    });
                })
                .catch(function (error) {
                    console.log(error)
                });
        }
    </script> --}}
@endsection
