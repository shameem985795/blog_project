@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        {{-- Cabinate Devisioin start --}}
        <div class="row mt-3">
            <div class="col-md-12 col-sm-12">
                <div class="card shadow rounded-card">
                    <div class="card-body">
                        <div class="card-title text-center">
                            <h4 class="header-title">{{ __('Cabinet Division (Grid view or list view)') }}</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Barisal Division') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Chittagong Division') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka Division') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Khulna Division') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Mymensingh Division') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Rajshahi Division') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Rangpur Division') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Sylhet Division') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            {{-- <h5>প্রতিবেদন অনুসারে খুজুন ( পিডিএফ )</h5> --}}
                            {{-- <div class="row text-center">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option value="">--প্রতিবেদন খুজুন--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-bordered-info">
                                        <i class="fa fa-search"></i>
                                        <span>অনুসন্ধান করুন</span>
                                    </button>
                                </div>
                            </div> --}}
                        </div>
                    </div> <!-- end card-body -->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        {{-- Cabinate Devisioin End --}}
{{-- Dhaka Devisioin start --}}
        {{-- <div class="row mt-3">
            <div class="col-md-12 col-sm-12">
                <div class="card shadow rounded-card">
                    <div class="card-body">
                        <div class="card-title text-center">
                            <h4 class="header-title">{{ __('Dhaka Division (Ongoing party report)') }}</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka District') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka District') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka District') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka District') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka District') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka District') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka District') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                            <div class="col-md-3 p-1">
                                <div class="card-box text-white text-center bg-primary" style="border-radius: 15px!important;">
                                    <h5 class="text-white">{{ __('Dhaka District') }}</h5>
                                    <p>প্রতিবেদন গৃহিত হয়েছে</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h5>প্রতিবেদন অনুসারে খুজুন ( পিডিএফ )</h5>
                            <div class="row text-center">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option value="">--প্রতিবেদন খুজুন--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-bordered-info">
                                        <i class="fa fa-search"></i>
                                        <span>অনুসন্ধান করুন</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end card-body -->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div> --}}
{{-- Dhaka Devisioin end --}}

{{--
        <div class="row mt-3">
            <div class="col-md-12 col-sm-12">
                <div class="card shadow rounded-card">
                    <div class="card-body">
                        <h4 class="header-title">{{ __('Latest Reports') }}</h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-centered mb-0">
                                <thead>
                                    <tr>
                                        <th>{{ __('Title') }}</th>
                                        <th>{{ __('Duration') }}</th>
                                        <th>{{ __('Completion') }} </th>
                                        <th>{{ __('List Info') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Lorem ipsum dolor sit amet</td>
                                        <td>4 days ago</td>
                                        <td>
                                            <div class="progress mb-2 progress-sm">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: 65%;" aria-valuenow="65" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="badge badge-primary">
                                                <i class="fas fa-exclamation-circle"></i>
                                                Details
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lorem ipsum dolor sit amet</td>
                                        <td>2 hours ago</td>
                                        <td>
                                            <div class="progress mb-2 progress-sm">
                                                <div class="progress-bar bg-warning" role="progressbar"
                                                    style="width: 65%;" aria-valuenow="65" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="badge badge-primary">
                                                <i class="fas fa-exclamation-circle"></i>
                                                Details
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lorem ipsum dolor sit amet</td>
                                        <td>1 days</td>
                                        <td>
                                            <div class="progress mb-2 progress-sm">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: 65%;" aria-valuenow="65" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="badge badge-primary">
                                                <i class="fas fa-exclamation-circle"></i>
                                                Details
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lorem ipsum dolor sit amet</td>
                                        <td>6 days ago</td>
                                        <td>
                                            <div class="progress mb-2 progress-sm">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: 65%;" aria-valuenow="65" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="badge badge-primary">
                                                <i class="fas fa-exclamation-circle"></i>
                                                Details
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lorem ipsum dolor sit amet</td>
                                        <td>12 hours ago</td>
                                        <td>
                                            <div class="progress mb-2 progress-sm">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: 65%;" aria-valuenow="65" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="badge badge-primary">
                                                <i class="fas fa-exclamation-circle"></i>
                                                Details
                                            </span>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div> <!-- end table responsive-->
                    </div> <!-- end card-body -->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card shadow division-bg rounded-card text-white">
                            <div class="card-body">
                                <div class="progressbar-content">
                                    <h5 class="text-white">{{ __('Divisional Report') }} </h5>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Submitted Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: 25%" aria-valuenow="25" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Unseen Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-danger" role="progressbar"
                                                    style="width: 100%" aria-valuenow="100"
                                                    aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Seen Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-info" role="progressbar"
                                                    style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ ('Draft Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-warning" role="progressbar"
                                                    style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card shadow district-bg rounded-card text-white">
                            <div class="card-body">
                                <div class="progressbar-content">
                                    <h5 class="text-white">{{ __('District Report') }} </h5>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Submitted Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: 25%" aria-valuenow="25" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Unseen Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-danger" role="progressbar"
                                                    style="width: 100%" aria-valuenow="100"
                                                    aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Seen Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-info" role="progressbar"
                                                    style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Draft Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-warning" role="progressbar"
                                                    style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-md-6 col-sm-12">
                <div class="card shadow procurement-graph rounded-card">
                    <div class="card-header bg-white">
                        <div class="my-2">
                            <table>
                                <tr>
                                    <td> {{ __('Divisional Report') }} <span>255</span></td>
                                    <td>{{ __('District Report') }}<span> 310</span></td>
                                    <td>{{ __('Upazila Report') }} <span>230</span></td>
                                    <td>{{ __('Union Report') }} <span>450</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <h6>Last Month <i class="fas fa-angle-down"></i></h6>
                        <div class="mt-1" dir="ltr">
                            <div id="chart" class="apex-charts"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card shadow upozilla-bg rounded-card text-white">
                            <div class="card-body">
                                <div class="progressbar-content">
                                    <h5 class="text-white">{{ __('Upazila Report') }} </h5>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Submitted Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: 25%" aria-valuenow="25" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Unseen Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-danger" role="progressbar"
                                                    style="width: 100%" aria-valuenow="100"
                                                    aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Seen Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-info" role="progressbar"
                                                    style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Draft Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-warning" role="progressbar"
                                                    style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card shadow union-bg rounded-card text-white">
                            <div class="card-body">
                                <div class="progressbar-content">
                                    <h5 class="text-white">{{ __('Union Report') }} </h5>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Submitted Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-success" role="progressbar"
                                                    style="width: 25%" aria-valuenow="25" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Unseen Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-danger" role="progressbar"
                                                    style="width: 100%" aria-valuenow="100"
                                                    aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Seen Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-info" role="progressbar"
                                                    style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <a class="nav-link" href="#">{{ __('Draft Report') }}</a>
                                        </div>
                                        <div class="col-sm-12 col-md-8 mt-2">
                                            <div class="progress progress-md">
                                                <div class="progress-bar bg-warning" role="progressbar"
                                                    style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-md-6 col-sm-12">
                <div class="card shadow procurement-graph rounded-card">
                    <div class="card-header bg-white">
                        <div class="my-2">
                            <table>
                                <tr>
                                    <td> {{ __('Divisional Report') }} <span>255</span></td>
                                    <td>{{ __('District Report') }}<span> 310</span></td>
                                    <td>{{ __('Upazila Report') }} <span>230</span></td>
                                    <td>{{ __('Union Report') }} <span>450</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="card-body p-2">
                        <h6>Last Month <i class="fas fa-angle-down"></i></h6>
                        <div class="mt-1" dir="ltr">
                            <div id="chart2" class="apex-charts"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row --> --}}

    </div> <!-- container -->
@endsection

@push('js')=
    <script src="{{ asset('libs/jquery-vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('libs/jquery-vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
@endpush
