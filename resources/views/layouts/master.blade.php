<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.header')
    <style>
        @import url('https://fonts.maateen.me/solaiman-lipi/font.css');
         * {
             font-family: 'solaimanlipi', Fallback, sans-serif;
         }
    </style>
    @stack('css')

</head>

<body>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        @include('partials.topbar')
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
        @include('partials.left-sidebar')
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <!-- Start Content-->
                @yield('content')

            </div> <!-- content -->

            <!-- Footer Start -->
            {{-- @include('partials.footer') --}}
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    <!-- Right Sidebar -->
    {{-- @include('partials.right-sidebar') --}}
    <!-- /Right-bar -->

    <!-- Right bar overlay-->
    {{-- @include('partials.rightbar-overlay') --}}







    <!-- Vendor js -->
    <script src="{{ asset('js/vendor.min.js') }}"></script> --}}

    <script src="{{ asset('libs/jquery-knob/jquery.knob.min.js') }}"></script>
    <script src="{{ asset('libs/peity/jquery.peity.min.js') }}"></script>

    <!-- Sparkline charts -->
    <script src="{{ asset('libs/jquery-sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- init js -->
    <script src="{{ asset('js/pages/dashboard-1.init.js') }}"></script>

    @stack('js')

    <!-- App js -->
    <script src="{{ asset('js/app.min.js') }}"></script>
</body>

</html>
