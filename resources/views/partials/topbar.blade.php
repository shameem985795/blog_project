<style>
    hr {
        margin: 0;
    }
</style>

<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">

        {{-- @if(count(auth()->user()->roles)>1)
            <li class="list-group-item">
                {!! Form::open([
                    'route'=>'change_active_role',
                    'id'=>'activeRoleChangeForm',
                    'method'=>'put'
                ]) !!}

                {!! Form::select('active_role_id', auth()->user()->roles->pluck('name', 'id'), auth()->user()->active_role_id, [
                    'class'=>'form-control',
                    'id'=>'activeRoleId',
                    'onchange'=>'changeActiveRole()'
                ]) !!}

                {!! Form::close() !!}
            </li>
        @endif --}}

        <li class="dropdown notification-list">
            {{-- <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button"
                aria-haspopup="false" aria-expanded="false">
                <i class="fe-bell noti-icon"></i>
                <span class="badge badge-danger rounded-circle noti-icon-badge">4</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="m-0">
                        <span class="float-right">
                            <a href="" class="text-dark">
                                <small>সব সরিয়ে ফেলুন</small>
                            </a>
                        </span>নোটিফিকেশন
                    </h5>
                </div>

                <div class="slimscroll noti-scroll">

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item active">
                        <div class="notify-icon bg-soft-primary text-primary">
                            <i class="mdi mdi-comment-account-outline"></i>
                        </div>
                        <p class="notify-details">
                            <small class="text-muted">1 min ago</small>
                        </p>
                    </a>

                    <!-- item-->
                </div>

                <!-- All-->
                <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                    সব প্রদর্শন করুন
                    <i class="fi-arrow-right"></i>
                </a>

            </div> --}}
        </li>

        <li class="dropdown notification-list">
            {{-- <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#"
                role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{ asset('images/users/avater.png') }}" alt="user-image" class="rounded-circle">
                <span class="pro-user-name ml-1">
                    {{-- {{ auth()->user()->ssoInfo()['designation']??auth()->user()->name }} <i class="mdi mdi-chevron-down"></i> --}}
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-header noti-title">
                    {{-- <h5 class="text-overflow m-0">স্বাগতম ! --}}
                        {{-- @if (isset(auth()->user()->activeRole)) --}}
                        {{-- &nbsp;<span style="color: rgb(14, 143, 202);text-transform: uppercase;font-weight: bold">{{ auth()->user()->activeRole->name }}</span> --}}
                        {{-- <h5 class="p-3 text-black"></h5> --}}
                        {{-- @endif --}}
                    </h5>
                </div>


                <hr>



                <!-- item-->
                {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="remixicon-account-circle-line"></i>
                    <span>প্রোফাইল</span>
                </a> --}}

{{--                @if(!auth()->user()->isAdmin())--}}
{{--                    <!-- item-->--}}
{{--                    <a  href="{{ route('report_info.index') }}" class="dropdown-item notify-item">--}}
{{--                        <i class="remixicon-account-circle-line"></i>--}}
{{--                        <span>My Reports</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a  href="{{ route('report_info.received_reports') }}" class="dropdown-item notify-item">--}}
{{--                        <i class="remixicon-account-circle-line"></i>--}}
{{--                        <span>Received Reports</span>--}}
{{--                    </a>--}}

{{--                    <!-- item-->--}}
{{--                    <a  href="{{ route('report_info.sent_reports') }}" class="dropdown-item notify-item">--}}
{{--                        <i class="remixicon-account-circle-line"></i>--}}
{{--                        <span>Sent Reports</span>--}}
{{--                    </a>--}}
{{--                @endif--}}

                <!-- item-->
                {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="remixicon-settings-3-line"></i>
                    <span>সেটিংস</span>
                </a>

                <div class="dropdown-divider"></div> --}}
                <!-- item-->
                {{-- <a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                    <i class="remixicon-logout-box-line"></i>
                    <span>লগ-আউট</span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </a> --}}

            </div>
        </li>


{{--        <li class="dropdown notification-list">--}}
{{--            <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">--}}
{{--                <i class="fe-settings noti-icon"></i>--}}
{{--            </a>--}}
{{--        </li>--}}


    </ul>

    {{-- <!-- LOGO -->
    <div class="logo-box">
        <a href="" class="logo text-center">
            <span class="logo-lg">
                <img src="{{ asset('images/rms/bd.png') }}" alt="" height="45">
                <!-- <span class="logo-lg-text-light">Xeria</span> -->
            </span>
            <span class="logo-sm">
                <!-- <span class="logo-sm-text-dark">X</span> -->
                <img src="{{ asset('images/rms/bd-sm.png') }}" alt="" height="38">
            </span>
        </a>
    </div> --}}

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            {{-- <button id="left-menu-toggle" class="button-menu-mobile waves-effect waves-light">
                <i class="fe-menu"></i>
            </button> --}}
        </li>

        <li class="dropdown d-none d-lg-block">
            <a class="nav-link dropdown-toggle waves-effect waves-light" style="font-size: 18px">
                {{-- {{ auth()->user()->ssoInfo()['office_ministry_name_bng']??'' }} --}}
            </a>
        </li>
    </ul>
    <ul class="list-unstyled topnav-menu topnav-menu justify-content-center">
        {{-- <h4 class="text-overflow float-center" style="margin: 28px 0px 0px 693px;color: rgba(255,255,255,.75)">স্বাগতম !
            @if (isset(auth()->user()->activeRole))
            &nbsp;<span style="text-transform: uppercase;">{{ auth()->user()->activeRole->name }}</span>
            @endif
        </h4> --}}
    </ul>
</div>

@push('js')
{{-- <script>
    function changeActiveRole() {
        // let roleId = $('#activeRoleId').val();
        // let url = window.location.protocol + "//" + window.location.host + "/roles/"+roleId+"/users";
        // window.location.replace(url);
        document.getElementById('activeRoleChangeForm').submit();
    }
</script> --}}
@endpush
