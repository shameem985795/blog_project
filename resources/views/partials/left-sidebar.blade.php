
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">
                {{-- <li>
                    <a href="{{ url('/') }}" class="waves-effect" >
                        <i class="remixicon-dashboard-fill"></i>
                        <span> {{ __("Dashboard") }} </span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/dashboard-2') }}" class="waves-effect" >
                        <i class="remixicon-bill-fill"></i>
                        <span> {{ __("Dashboard 2") }} </span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('data-visualization') }}" class="waves-effect">
                        <i class="remixicon-pages-line"></i>
                        <span>{{ __('Data Visualization') }} </span>
                    </a>
                </li> --}}

                <li class="border-bottom">
                    <a href="{{ url('/report-template-builder') }}" class="waves-effect">
                        <i class="remixicon-pages-line"></i>
                        <span>{{ __('রিপোর্ট টেম্পলেট বিল্ডার') }}</span>
                    </a>
                </li>
{{--                <li>--}}
{{--                    <a href="javascript: void(0);" class="waves-effect">--}}
{{--                        <i class="remixicon-pages-line"></i>--}}
{{--                        <span>{{ __('Report Module') }} </span>--}}
{{--                        <span class="menu-arrow"></span>--}}
{{--                    </a>--}}
{{--                    <ul class="nav-second-level" aria-expanded="false">--}}
{{--                        <li>--}}
{{--                            <a href="#"> {{ __('Add Report') }} </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#"> {{ __('View Report') }} </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#"> {{ __('Report Permission') }} </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

                <!-- <li>
                    <a href="" class="waves-effect">
                        <i class="fa fa-file"></i>
                        <span>{{ __('Report Studio') }} (a)</span>
                    </a>
                </li>

                <li>
                    <a href="" class="waves-effect">
                        <i class="fa fa-box"></i>
                        <span>{{ __('Report Studio') }} (1)</span>
                    </a>
                </li>

                <li>
                    <a href="" class="waves-effect">
                        <i class="fa fa-box"></i>
                        <span>{{ __('Report Studio 2') }} (p)</span>
                    </a>
                </li> -->

                {{-- <li>
                    <a href="" class="waves-effect">
                        <i class="fa fa-box"></i>
                        <span>ইউজার পদবি ভূমিকা</span>
                    </a>
                </li> --}}

                {{-- <li class="border-top pt-2">
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="remixicon-group-fill"></i>
                        <span>{{ __('User Management') }} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="#"> {{ __('User Profile') }} </a>
                        </li>
                    </ul>
                </li>
                <li> --}}
                    {{-- @if(!auth()->user()->isAdmin())
                    <!-- item-->
                        <a  href="{{ route('report_info.index') }}" class="dropdown-item notify-item">
                            <i class="fa fa-book"></i>
                            <span>নিজের রিপোর্ট </span>
                        </a>

                        <!-- item-->
                        <a  href="{{ route('report_info.received_reports') }}" class="dropdown-item notify-item">
                            <i class="fa fa-book"></i>
                            <span> আগত রিপোর্ট </span>
                        </a>

                        <!-- item-->
                        <a  href="{{ route('report_info.sent_reports') }}" class="dropdown-item notify-item">
                            <i class="fa fa-book"></i>
                            <span> প্রেরিত রিপোর্ট </span>
                        </a>
                    @endif --}}
                </li>
                {{-- <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="remixicon-notification-3-fill"></i>
                        <span class="badge badge-success badge-pill float-right">2</span>
                        <span>{{ __('Notification') }} </span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="" class="waves-effect">
                                <i class="remixicon-pages-line"></i>
                                <span>{{ __('Notifications') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="#"> {{ __('Configure Report Notification') }} </a>
                        </li>
                        <li>
                            <a href="#"> {{ __('Configure Form Permission Notification') }} </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="" class="waves-effect">
                        <i class="fa fa-box"></i>
                        <span>{{ __('Components') }}</span>
                    </a>
                </li>
                <!-- <li>
                    <a href="" class="waves-effect">
                        <i class="remixicon-pages-line"></i>
                        <span>{{ __('Organograms') }}</span>
                    </a>
                </li> -->
                <li>
                    <a href="" class="waves-effect">
                        <i class="remixicon-pages-line"></i>
                        <span>{{ __('API') }}</span>
                    </a>
                </li>

{{--                <li>--}}
{{--                    <a href="{{ route('simples.index') }}" class="waves-effect">--}}
{{--                        <i class="remixicon-pages-line"></i>--}}
{{--                        <span>{{ __('Simples') }}</span>--}}
{{--                    </a>--}}
{{--                </li>--}}


            {{-- <li class="border-top pt-2">
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="fa fa-book"></i>
                        <span> {{ __('Report') }}  </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href=""> {{ __('Report Template') }} </a>
                        </li>
                        <li>
                            <a href=""> {{ __('Report Description') }} </a>
                        </li>
                        <li>
                            <a href=""> {{ __('Reporrt Columns') }} </a>
                        </li>
                        <li>
                            <a href=""> {{ __('Report Column Group') }} </a>
                        </li>
                        <li>
                            <a href=""> {{ __('Report Rows') }} </a>
                        </li>
                        <li>
                            <a href=""> {{ __('Report Row Groups') }} </a>
                        </li>
                        <li>
                            <a href=""> {{ __('All Report Create') }} </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="" class="waves-effect">
                        <i class="fa fa-box"></i>
                        <span>Union Maker</span>
                    </a>
                </li> --}}
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
