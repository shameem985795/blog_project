@extends('layouts.master')

@section('content')
<div class="container-fluid mt-2">
	<div class="row">
		<div class="col-3">
			<form action="" method="post">
				@csrf
				<input type="text" name="id" value="{{ $reportTemplate->id }}" class="d-none">
				<div class="stick">
					<ul class="nav nav-tabs">
						<li class="nav-item">
							<a href="#standard" data-toggle="tab" aria-expanded="true" class="nav-link active">
								<span class="d-inline-block d-sm-none"><i class="fas fa-home"></i></span>
								<span class="d-none d-sm-inline-block">Standard</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="#custom" data-toggle="tab" aria-expanded="false" class="nav-link">
								<span class="d-inline-block d-sm-none"><i class="far fa-user"></i></span>
								<span class="d-none d-sm-inline-block">Custom</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="#exp-data" data-toggle="tab" aria-expanded="false" class="nav-link">
								<span class="d-inline-block d-sm-none"><i class="far fa-user"></i></span>
								<span class="d-none d-sm-inline-block">Data</span>
							</a>
						</li>
					</ul>
					<div class="tab-content mt-0 pt-0">
						<div class="tab-pane fade show active" id="standard">
							<div id="STANDARD-FORM-ELEMENTS" class="list-group"></div>
						</div>
						<div class="tab-pane fade" id="custom">
							<div id="CUSTOM-FORM-ELEMENTS" class="list-group"></div>
						</div>
						<div class="tab-pane fade" id="exp-data">
							<textarea id="exported-json" name="standard_form_json" class="form-control exported-json" rows="10" readonly></textarea>
							<textarea id="existing-json" class="form-control">{{ $reportTemplate->standard_form_json }}</textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group text-right ml-1">
						<button type="submit" id="exportXSD" class="btn btn-primary mt-2 mb-2">
							<i class="fa fa-save"></i> SAVE
						</button>
						<button type="button" id="clear-form" class="btn btn-danger mt-2 mb-2">
							<i class="far fa-trash-alt"></i> CLEAR FORM
						</button>
					</div>
				</div>
			</form>
		</div> <!-- .col-3 -->






		<div class="col-6">
			<div class="card-box" style="height: 850px; overflow:auto;">
				<div id="formElementsField" class="xlist-group"></div>
			</div>
		</div> <!-- .col-6 -->








		<div class="col-3">
			<div id="editor-pan" class="card stick">
				<div class="card-header">
					<div class="row">
						<div class="col-9">
							<h4 class="mt-0 mb-0"><i class="fas fa-pencil-alt"></i> Element Editor</h4>
						</div>
						<div class="col-3 text-right">
							<button title="Reload" id="reload-editor" class="custom-btn-link ml-1"><i class="fa fa-sync text-purple"></i></button>
							<button title="Close" id="close-editor" class="custom-btn-link ml-1"><i class="far fa-times-circle fa-lg text-danger"></i></button>
						</div>
					</div>
				</div>
				<div id="editor-field" class="card-body" style="min-height: 500px;">
					<div class="alert alert-warning text-center mt-5">
						<p class="h3">Please select an input field to edit element options.</p>
					</div>
				</div>
				<div class="card-footer">
					<button type="button" id="submit-edit" class="btn btn-secondary btn-sm float-right"><i class="fa fa-check"></i> Update</button>
				</div>
			</div>



		</div>

	</div>

</div>


<div id="POPUP" class="modal fade" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">EXPORTED JSON DATA</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<textarea class="form-control exported-json" rows="10" readonly></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" id="copy-json-to-clipboard" class="btn btn-primary"><i class="far fa-clone"></i> Copy to Clipboard</button>
			</div>
		</div>
	</div>
</div>

@endsection


@push('css')
<style>
	.handle, .drag-handle {
		cursor: move;
	}

	.blue-background-class {
		background-color: skyblue;
	}

	.stick {
		top: 10px;
		position: sticky;
	}

	#EL_OPTIONS{
		min-height: 50px;
		max-height: 300px;
		overflow: auto;
	}

	.form-element-container{
		border: 1px solid #EEE;
		padding:10px 5px;
	}
	.form-element-container.active{
		background-color: yellow;
	}

	.custom-btn-link{
		padding:0px;
		margin: 0px;
		background-color: transparent;
		border: none;
	}

	.form-element-control-btn{
		padding:0px;
		margin-right: 3px;
		background-color: transparent;
		border: none;
		transition: 0.5s;
	}
	.form-element-control-btn:hover{
		transform: scale(1.25);
	}
</style>

@endpush

@push('js')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
	integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/sortablejs@1.10.2/Sortable.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/file-saver@2.0.2/dist/FileSaver.min.js"></script>
<script src="{{ asset('js/form-studio.js') }}"></script>

<script>
	(function($){
        $(document).ready(function(){
        });
    })(jQuery)
</script>
@endpush
