<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigureReportOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configure_report_office', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('report_template_id');
            $table->string('office_layer_id')->nullable();
            $table->json('office_id')->nullable();
            $table->string('office_unit_id')->nullable();
            $table->string('office_origin_id')->nullable();
            $table->string('office_origin_unit_id')->nullable();
            $table->string('office_ministry_id')->nullable();
            $table->string('office_directorate_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configure_report_office');
    }
}
