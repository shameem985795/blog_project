<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTemplateDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_template_descriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('report_template_id');
            $table->string('title_en')->nullable();
            $table->string('title_bn')->nullable();
            $table->string('title_position', 32)->nullable();
            $table->string('subtitle_en')->nullable();
            $table->string('subtitle_bn')->nullable();
            $table->string('subtitle_position', 32)->nullable();
            $table->string('label_en')->nullable();
            $table->string('label_bn')->nullable();
            $table->string('attribute_en')->nullable();
            $table->string('attribute_bn')->nullable();
            $table->string('header_left_component')->nullable();
            $table->string('header_center_component')->nullable();
            $table->string('header_right_component')->nullable();
            $table->string('header_description')->nullable();
            $table->string('footer_left')->nullable();
            $table->string('footer_center')->nullable();
            $table->string('footer_right')->nullable();
            $table->string('footer_description')->nullable();
            $table->json('type')->nullable();
            $table->string('short_info')->nullable();
            $table->tinyInteger('has_column_groups')->default(0);
            $table->json('visible_columns')->nullable();
            $table->smallInteger('total_columns')->default(0);
            $table->tinyInteger('has_row_groups')->default(0);
            $table->tinyInteger('has_row_labels')->default(0);
            $table->tinyInteger('has_row_footer')->default(0);
            $table->smallInteger('total_min_rows')->default(0);
            $table->json('owner')->nullable();
            $table->json('definition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_template_descriptions');
    }
}
