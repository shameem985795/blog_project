<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_templates', function (Blueprint $table) {
            $table->id();
            $table->string('title_en')->nullable();
            $table->string('title_bn');
            $table->string('version', 32)->nullable();
            $table->text('description')->nullable();
            $table->json('owner')->nullable();
            $table->json('definition')->nullable();
            $table->tinyInteger('is_standard')->default(0);
            $table->json('standard_form_json')->nullable();
            $table->json('standard_db_json')->nullable();
            $table->json('standard_report_json')->nullable();
            $table->string('standard_action_url', 64)->nullable();
            $table->string('standard_form_name', 64)->nullable();
            $table->string('storage_name', 32)->nullable();
            $table->string('migration_file_name', 128)->nullable();
            $table->tinyInteger('is_active');
            $table->tinyInteger('is_configured')->default(0);
            $table->dateTime('configured_at')->nullable();
            $table->dateTime('applied_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_templates');
    }
}
