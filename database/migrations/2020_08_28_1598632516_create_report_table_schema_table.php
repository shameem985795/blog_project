<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTableSchemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('report_table_schema', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ministry_id');
            $table->integer('directorate_id');
            $table->integer('user_id');
            $table->string('report_name', 150);
            $table->text('report_header');
            $table->longText('report_column');
            $table->text('report_footer');
            $table->text('column_group');
            $table->enum('report_type', ['Summary Report', 'Detail Report'])->default('Summary Report');
            $table->timestamps(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('report_table_schema');
    }
}