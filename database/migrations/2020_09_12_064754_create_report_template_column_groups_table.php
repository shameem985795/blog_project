<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTemplateColumnGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_template_column_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('report_template_id');
            $table->string('column_group_label')->nullable();
            $table->string('column_group_label_attribute')->nullable();
            $table->integer('start_column_id')->nullable();
            $table->integer('end_column_id')->nullable();
            $table->smallInteger('column_group_label_sequance')->nullable();
            $table->smallInteger('number_of_columns')->nullable();
            $table->json('owner')->nullable();
            $table->json('definition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_template_column_groups');
    }
}
