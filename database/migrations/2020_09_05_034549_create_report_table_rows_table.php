<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTableRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_table_rows', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('report_templete_id');
            $table->string('column_id');
            $table->string('row_column_value');
            $table->timestamps();

//            $table->foreign('report_templete_id')->references('id')->on('report_templetes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_table_rows');
    }
}
