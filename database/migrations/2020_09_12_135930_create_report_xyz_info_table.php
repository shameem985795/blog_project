<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportXyzInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_xyz_info', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('report_template_id')->nullable();
            $table->string('title_en')->nullable();
            $table->string('title_bn')->nullable();
            $table->json('report_template_detail')->nullable();
            $table->string('office_id')->nullable();
            $table->string('office_origin_id')->nullable();
            $table->string('office_unit_id')->nullable();
            $table->string('office_origin_unit_id')->nullable();
            $table->string('office_layer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_xyz_info');
    }
}
