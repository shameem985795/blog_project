<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigureReportScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('configure_report_schedule', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('report_template_id');

            $table->boolean('is_adhoc')->default(false);
            $table->date('adhoc_due_date')->nullable();
            $table->integer('adhoc_grace_period')->nullable();

            $table->boolean('is_recurring')->default(false);
            $table->string('recurring_type')->nullable();

            $table->string('recurring_weekly')->nullable();
            $table->integer('recurring_weekly_grace_period')->nullable();

            $table->string('recurring_biweekly')->nullable();
            $table->integer('recurring_biweekly_grace_period')->nullable();

            $table->string('recurring_monthly')->nullable();
            $table->integer('recurring_monthly_grace_period')->nullable();

            $table->string('recurring_quarterly')->nullable();
            $table->integer('recurring_quarterly_grace_period')->nullable();

            $table->string('recurring_halfyearly')->nullable();
            $table->integer('recurring_halfyearly_grace_period')->nullable();

            $table->integer('recurring_yearly')->nullable();
            $table->integer('recurring_yearly_grace_period')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configure_report_schedule');
    }
}
