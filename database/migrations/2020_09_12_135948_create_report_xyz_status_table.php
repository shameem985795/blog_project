<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportXyzStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('report_xyz_status', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('report_template_id')->nullable();
            $table->unsignedBigInteger('report_xyz_info_id')->nullable();
            $table->date('report_due_date')->nullable();
            $table->date('report_graced_date')->nullable();

            $table->string('current_status')->nullable();

            $table->unsignedBigInteger('maker_id')->nullable();
            $table->dateTime('maked_date')->nullable();
            $table->string('maker_designation_id')->nullable();

            $table->unsignedBigInteger('checker_id')->nullable();
            $table->dateTime('checked_date')->nullable();
            $table->string('checker_designation_id')->nullable();

            $table->unsignedBigInteger('approver_id')->nullable();
            $table->dateTime('approved_date')->nullable();
            $table->string('approver_designation_id')->nullable();
            $table->json('to_info')->nullable();
            $table->json('from_info')->nullable();

            $table->boolean('is_handled')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_xyz_status');
    }
}
