<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTemplateRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_template_rows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('report_template_id');
            $table->unsignedBigInteger('report_template_row_group_id')->nullable();
            $table->string('row_label')->nullable();
            $table->json('row_label_attribute')->nullable();
            $table->json('owner')->nullable();
            $table->json('definition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_template_rows');
    }
}
