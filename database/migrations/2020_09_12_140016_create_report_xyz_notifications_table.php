<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportXyzNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('report_xyz_notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('report_xyz_info_id')->nullable();
            $table->string('notify_by')->nullable();
            $table->integer('notification_number')->nullable();
            $table->date('notification_date')->nullable();
            $table->dateTime('notified_at')->nullable();
            $table->string('notification_status')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_xyz_notifications');
    }
}
