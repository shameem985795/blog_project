<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTemplateColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_template_columns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('report_template_id');
            $table->unsignedBigInteger('column_group_id')->nullable();
            $table->string('column_name_en')->nullable();
            $table->string('column_name_bn')->nullable();
            $table->string('column_name_en_db')->nullable();
            $table->string('column_name_style')->nullable();
            $table->string('column_input_type', 32)->nullable();
            $table->string('column_default_value')->nullable();
            $table->string('column_row_sum')->nullable();
            $table->string('column_rule')->nullable();
            $table->string('column_foot_rule')->nullable();
            $table->string('column_alpha_identifier', 16)->nullable();
            $table->smallInteger('column_numeric_identifier')->nullable();
            $table->smallInteger('column_order')->nullable();
            $table->tinyInteger('is_visible_on_report')->default(1);
            $table->json('owner')->nullable();
            $table->json('definition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_template_columns');
    }
}
