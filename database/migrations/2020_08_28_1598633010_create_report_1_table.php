<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReport1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('report_1', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->text('col1');
            $table->text('col2');
            $table->text('col3');
            $table->text('col4');
            $table->text('col5');
            $table->text('col6');
            $table->text('col7');
            $table->longText('col_settings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('report_1');
    }
}