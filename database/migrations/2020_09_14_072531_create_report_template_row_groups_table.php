<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTemplateRowGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_template_row_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('report_template_id');
            $table->string('row_group_label')->nullable();
            $table->json('row_group_label_attribute')->nullable();
            $table->smallInteger('number_of_rows')->nullable();
            $table->json('owner')->nullable();
            $table->json('definition')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_template_row_groups');
    }
}
