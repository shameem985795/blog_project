<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportXyzMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('report_xyz_movements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('report_xyz_id')->nullable();
            $table->unsignedBigInteger('report_template_id')->nullable();

            $table->string('from_office_id')->nullable();
            $table->string('from_office_origin_id')->nullable();
            $table->string('from_office_unit_id')->nullable();
            $table->string('from_office_origin_unit_id')->nullable();
            $table->string('from_office_layer_id')->nullable();

            $table->string('to_office_id')->nullable();
            $table->string('to_office_origin_id')->nullable();
            $table->string('to_office_unit_id')->nullable();
            $table->string('to_office_origin_unit_id')->nullable();
            $table->string('to_office_layer_id')->nullable();

            $table->string('from_designation_id')->nullable();
            $table->unsignedBigInteger('from_user_id')->nullable();
            $table->string('from_user_name')->nullable();
            $table->string('from_officer_signature')->nullable();
            $table->string('to_designation_id')->nullable();
            $table->unsignedBigInteger('to_user_id')->nullable();
            $table->string('to_user_name')->nullable();
            $table->string('from_status')->nullable();
            $table->string('to_status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_xyz_movements');
    }
}
