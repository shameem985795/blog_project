<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::group(['prefix' => 'report-template-builder'], function () {
    Route::get('/','ReportTemplateController@index')->name('rtb.index');
    Route::get('/create', 'ReportTemplateController@create')->name('rtb.create');
    Route::post('/store', 'ReportTemplateController@store')->name('rtb.store');
    Route::get('/view/{id}', 'ReportTemplateController@view')->name('rtb.view');
    Route::get('/edit/{id}', 'ReportTemplateController@edit')->name('rtb.edit');
    Route::post('/update/{id}', 'ReportTemplateController@update')->name('rtb.update');
    Route::get('/standard-editor/{id}', 'ReportTemplateController@standardEditor')->name('rtb.standard-editor');

});
