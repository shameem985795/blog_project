<?php

namespace App\Http\Controllers;

use Exception;
use App\ReportTemplate;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ReportTemplateController extends Controller
{
    public function index()
    {
        $reportTemplates = ReportTemplate::orderBy('id', 'desc')->paginate(25);

        return view('report-template-builder.index',compact('reportTemplates'));
    }

    public function create()
    {
        return view('report-template-builder.create');
    }


    private function generateRandomTableName()
    {
        $alphabetCount = 6;

        $randomAlphabets = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, $alphabetCount);

        $randomNumbers = rand(100, 10000);

        return 'report_' . Str::plural($randomAlphabets . $randomNumbers);
    }


    public function store(Request $request)
    {
    try
        {
            if (empty(config('app.api_url')))
            {
                return redirect()
                    ->route('rtb.create')
                    ->withInput()
                    ->withErrors('API Engine URL Not Found!');
            }

            $reportTemplate = ReportTemplate::create([
                'title_en'                  =>  $request->title_bn,
                'title_bn'                  =>  $request->title_bn,
                'description'               =>  $request->description,
                'storage_name'              =>  $this->generateRandomTableName(),
                'standard_action_url'       =>  config('app.api_url') . '/datamodel/store',
                'is_active'                 =>  isset($request->is_active) ? 1 : 0,
            ]);

            return redirect()
                    ->route('rtb.index')
                    ->withMessage('Report has been added successfully!');
        } catch (Exception $e)
        {
            return redirect()
                    ->route('rtb.create')
                    ->withInput()
                    ->withErrors($e->getMessage());
        }
    }


    public function view($id)
    {
        $reportTemplate = ReportTemplate::findOrFail($id);

        return view('report-template-builder.view', compact('reportTemplate'));
    }

    public function edit($id)
    {
        $reportTemplate = ReportTemplate::findOrFail($id);

        return view('report-template-builder.edit', compact('reportTemplate'));
    }

    public function update(Request $request, $id)
    {
        try
        {
            $reportTemplate                     =   ReportTemplate::findOrFail($id);

            $reportTemplate->title_bn           = $request->title_bn;
            $reportTemplate->description        = $request->description;
            $reportTemplate->is_active          = isset($request->is_active) ? 1 : 0;

            $reportTemplate->save();

            return redirect()
                    ->route('rtb.index')
                    ->withMessage('Report has been saved successfully!');
        } catch (Exception $e)
        {
            return redirect()
                    ->route('rtb.edit', $id)
                    ->withInput()
                    ->withErrors($e->getMessage());
        }
    }

    public function standardEditor($id)
    {
        $reportTemplate = ReportTemplate::findOrFail($id);

        return view('form-studio.create', compact('reportTemplate'));
    }










}
