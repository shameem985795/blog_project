
let feedData_for_rebuild = [{"title":"Logo","description":"Please select an option","icon":"<i class=\"fa fa-image\"></i>","label":"Logo","type":"image","defaultImage":"http://localhost.dorms/images/form-studio/gov.bd.png","height":124,"width":124,"uid":"dd_1599965282488","attributes":{"class":""},"labelText":"Logo"},{"title":"Text Area","description":"You can type some big text here...","icon":"<i class=\"fas fa-paragraph\"></i>","label":"Some string input field","type":"text","required":false,"category":"","uid":"ID1599960356700","labelText":"Some string input field","attributes":{"class":"form-control","placeholder":"Hello imran","autofocus":false,"readonly":false,"disabled":false}},{"title":"Radio Buttons","description":"Please select an option","icon":"<i class=\"fas fa-dot-circle\"></i>","label":"Drop-down menu sdsd sds s sds ","type":"radio_buttons","options":["Option 1","Hello","Option 2","Aminul","option 3"],"required":false,"uid":"ID1599960356715","labelText":"Drop-down menu sdsd sds s sds ","attributes":{"class":"","placeholder":"-Please select an option-","autofocus":false,"readonly":false,"disabled":false}},{"title":"Heading","description":"Please type a string","icon":"<i class=\"fas fa-heading\"></i>","label":"Text Input","type":"text","required":false,"category":"","uid":"ID1599960356729","labelText":"Text Input","attributes":{"class":"form-control","placeholder":"Abcd Efgh","autofocus":false,"readonly":false,"disabled":false}},{"title":"Text Input","description":"Please type a string","icon":"<i class=\"fas fa-text-height\"></i>","label":"Text Input","type":"text","required":false,"category":"","uid":"ID1599960356741","labelText":"Text Input","attributes":{"class":"form-control","placeholder":"Abcd Efgh","readonly":false,"disabled":false}},{"title":"Text Area","description":"You can type some big text here...","icon":"<i class=\"fas fa-paragraph\"></i>","label":"Text Input sdf sdf s","type":"textarea","required":false,"category":"","uid":"ID1599960356757","labelText":"Text Input sdf sdf s","attributes":{"class":"form-control","placeholder":"Hello world,\nHow are you?...","autofocus":false,"readonly":false,"disabled":false}}];

 


const STANDARD_FORM_ELEMENTS = [
 
    {
        "title"         : "Text Input",
        "description"	: "Please type a string",
        "icon"          : "<i class=\"fas fa-text-height\"></i>",
        "label"         : "Text Input",
        "type"          : "text",
        "placeholder"	: "Abcd Efgh",
        "idName"	    : "Text_",
        "className"	    : "form-control",
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false,
        "category"      :''
    },
 
    {
        "title"         : "Heading",
        "description"	: "Please type a string",
        "icon"          : "<i class=\"fas fa-heading\"></i>",
        "label"         : "Text Input",
        "type"          : "text",
        "placeholder"	: "Abcd Efgh",
        "idName"	    : "Text_",
        "className"	    : "form-control",
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false,
        "category"      :''
    },
 
    {
        "title"         : "Text Area",
        "description"	: "You can type some big text here...",
        "icon"          : "<i class=\"fas fa-paragraph\"></i>",
        "label"         : "Text Input",
        "type"          : "textarea",
        "placeholder"	: "Hello world,\nHow are you?...",
        "idName"	    : "TextArea_",
        "className"	    : "form-control",
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false,
        "category"      :''
    },

    {
        "title"         : "Numeric Input",
        "description"	: "Please type a number",
        "icon"          : "<i class=\"fa fa-clock\"></i>",
        "label"         : "Numeric Input",
        "type"          : "number",
        "placeholder"	: "0.00",
        "idName"	    : "Num_",
        "className"	    : "form-control",
        "min"           : 0,
        "max"           : 100,
        "step"          : 10,
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false
    },

    {
        "title"         : "Drop-down",
        "description"	: "Please select an option",
        "icon"          : "<i class=\"fa fa-check\"></i>",
        "label"         : "Drop-down",
        "type"          : "select",
        "placeholder"	: "-Please select an option-",
        "idName"	    : "dd_",
        "className"	    : "form-control",
        "options"       : ['Option 1', 'Option 2', 'option 3'],
        "multiple"      : false,
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false
    },

    {
        "title"         : "Drop-down Multi-select",
        "description"	: "Please select an option",
        "icon"          : "<i class=\" fas fa-check-double\"></i>",
        "label"         : "Drop-down menu",
        "type"          : "select",
        "placeholder"	: "-Please select an option-",
        "idName"	    : "dd_",
        "className"	    : "form-control",
        "options"       : ['Option 1', 'Option 2', 'option 3'],
        "multiple"      : true,
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false
    },
    
    {
        "title"         : "Radio Buttons",
        "description"	: "Please select an option",
        "icon"          : "<i class=\"fas fa-dot-circle\"></i>",
        "label"         : "Drop-down menu",
        "type"          : "radio_buttons",
        "placeholder"	: "-Please select an option-",
        "idName"	    : "dd_",
        "className"	    : "",
        "options"       : ['Option 1', 'Option 2', 'option 3'],
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false
    },

    {
        "title"         : "Check Boxes",
        "description"	: "Please select an option",
        "icon"          : "<i class=\"far fa-check-square\"></i>",
        "label"         : "Drop-down menu",
        "type"          : "check_boxes",
        "placeholder"	: "-Please select an option-",
        "idName"	    : "dd_",
        "className"	    : "",
        "options"       : ['Imran 1', 'Mohiuddin 2', 'option 3'],
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false
    }
    
];
const CUSTOM_FORM_ELEMENTS = [

    {
        "title"         : "Logo",
        "description"	: "Please select an option",
        "icon"          : "<i class=\"fa fa-image\"></i>",
        "label"         : "Logo",
        "type"          : "image",
        "defaultImage"	: window.location.origin+"/images/form-studio/gov.bd.png",
        "height"        : 124,
        "width"         : 124,
        "idName"	    : "dd_",
        "className"	    : ""
    },

    {
        "title"         : "Heading Element",
        "icon"          : "<i class=\"fas fa-heading\"></i>",
        "label"         : "Heading Element",
        "type"          : "heading_element",
        "default_value" : "গণপ্রজাতন্ত্রী বাংলাদেশ সরকার\nজনপ্রশাসন মন্ত্রণালয়",
        "idName"	    : "dd_",
        "className"	    : "form-control text-center"
    },
    

    {
        "title"         : "Division",
        "description"	: "Please select a division",
        "icon"          : "<i class=\" fas fa-map-marker\"></i>",
        "label"         : "Division",
        "type"          : "select",
        "placeholder"	: "-Please select a division-",
        "idName"	    : "division_",
        "className"	    : "form-control",
        "options"       : ['Dhaka', 'Chattagram', 'Barisal', 'Khulna', 'Rangpur', 'Mymensingh', 'Rajshahi', 'Sylhet'],
        "multiple"      : false,
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false
    },
    

    {
        "title"         : "Row Labels",
        "description"	: "Please select a division",
        "icon"          : "<i class=\" fas fa-map-marker\"></i>",
        "label"         : "Row Labels",
        "type"          : "select",
        "placeholder"	: "-Please add Row Labels-",
        "idName"	    : "Row-Labels",
        "className"	    : "form-control",
        "options"       : ['Row 1', 'Row 2', 'Row 3'],
        "multiple"      : false,
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false
    },
    

    {
        "title"         : "Districts",
        "description"	: "Please select a district",
        "icon"          : "<i class=\" fas fa-map-marker\"></i>",
        "label"         : "District",
        "type"          : "select",
        "placeholder"	: "-Please select a district-",
        "idName"	    : "district_",
        "className"	    : "form-control",
        "options"       : ['Dhaka', 'Chattagram', 'Barisal', 'Khulna', 'Rangpur', 'Mymensingh', 'Rajshahi', 'Sylhet'],
        "multiple"      : false,
        "autofocus"	    : false,
        "required"	    : false,
        "readonly"	    : false,
        "disabled"	    : false
    },
    
    {
        "title"         : "Heading 1-6",
        "icon"          : "<i class=\"fas fa-heading\"></i>",
        "label"         : "Heading Element",
        "type"          : "h1",
        "tag_options"   : ['h1','h2','h3','h4','h5','h6'],
        "default_value" : "গণপ্রজাতন্ত্রী বাংলাদেশ সরকার",
        "idName"	    : "dd_",
        "className"	    : "text-center"
    }

];


(function($){
    $(document).ready(function(){
        $('#left-menu-toggle').trigger('click');
        $(document).find('.init-tooltip').tooltip();
        $('#submit-edit').prop('disabled', true);




        new Sortable(formElementsField, {
            animation: 150,
            handle: '.handle', // handle's class
            ghostClass: 'blue-background-class'
        });

        
        import_data();


        // initializing left buttons
        let 
            standard_form_elements = '',
            custom_form_elements = '';
        $.each(STANDARD_FORM_ELEMENTS, function(i,form_item){
            standard_form_elements += `
                <button title="Click to add item" type="button" class="list-group-item list-group-item-action form-item-insertion-trigger" data-formjson='${JSON.stringify(form_item)}'>
                    <span class="mr-1">${form_item.icon}</span> <span>${form_item.title}</span> 
                </button>
            `;
        });
        $.each(CUSTOM_FORM_ELEMENTS, function(i,form_item){
            custom_form_elements += `
                <button title="Click to add item" type="button" class="list-group-item list-group-item-action form-item-insertion-trigger" data-formjson='${JSON.stringify(form_item)}'>
                    <span class="mr-1">${form_item.icon}</span> <span>${form_item.title}</span> 
                </button>
            `;
        });
        $('#STANDARD-FORM-ELEMENTS').html(standard_form_elements);
        $('#CUSTOM-FORM-ELEMENTS').html(custom_form_elements);
        // end : initializing left buttons



        $(document).on('click', '.form-item-insertion-trigger', function(e){
            let 
                data = $(this).data(),
                html = new FormElement(data.formjson);
            $('#formElementsField').append(html);
        });

        
        $(document).on('click', '.form-element-control-btn-edit', function(e){
            let 
                uid     = $(this).parent().data('uid'),
                edit    =  new ElementEditorPanel(uid);
        });


        $(document).on('click', '.form-element-control-btn-remove', function(e){
            let 
                uid     = $(this).parent().data('uid'),
                element = $(document).find(`.form-element-container[data-uid=${uid}]`);

            element.addClass('bg-danger');
            
            if(confirm('Are you sure deleting this element?'))
            {
                reset_editor_pan();
                $.when(element.fadeOut('slow')).done(function(){
                    element.remove();
                    export_data();
                });
            }
            else
            {
                element.removeClass('bg-danger');
            }
            
        });


        $(document).on('click', '#close-editor', function(e){
            reset_editor_pan();
        });


        $(document).on('click', '.option-control-btn', function(e){
            let 
                data    = $(this).data(),
                el      = $(this).closest('.option-control'),
                new_opt = `
                    <div class="input-group mb-1 option-control" id="${NOW()}">
                        <div class="input-group-prepend drag-handle">
                            <span title="Drag to move order" class="input-group-text"><i class="fa fa-bars"></i></span>
                        </div>
                        <input type="text" class="edit-element-control edit-options form-control" data-target="options">
                        <div class="input-group-append">
                            <button title="Remove this option" class="option-control-btn btn btn-outline-danger" type="button" data-optionControl="remove"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="input-group-append">
                            <button title="Add option before this" class="option-control-btn btn btn-outline-success p-1" type="button" data-optionControl="add-before"><i class="fa fa-arrow-up"></i> <i class="fa fa-plus"></i></button>
                        </div>
                        <div class="input-group-append">
                            <button title="Add option after this" class="option-control-btn btn btn-outline-success p-1" type="button" data-optionControl="add-after"><i class="fa fa-plus"></i> <i class="fa fa-arrow-down"></i></button>
                        </div>
                    </div>
                `;
            console.log(data);
            if(data.optioncontrol=='remove'){
                if($(document).find('.option-control').length>1)
                {
                    $.when(el.fadeOut('slow')).done(function(){
                        el.remove();
                    });
                }else{
                    alert("You cannot remove all the options!\nAt least one option must be kept!");
                }
            }
            else if(data.optioncontrol=='add-before'){
                el.before(new_opt);
            }
            else if(data.optioncontrol=='add-after'){
                el.after(new_opt);
            }
        });


        $(document).on('click', '#reload-editor', function(){
            $(document).find('.form-element-container.active .form-element-control-btn-edit').trigger('click');
        });


        $(document).on('click', '#export', function(e){

            let elements = $(document).find('.form-element-container');
    
            if(!(elements.length>0))
            {
                alert('NO ELEMENTS!');
                return null;
            }
            export_data();
        });


        $(document).on('click', '#copy-json-to-clipboard', function(e) {
            /* Get the text field */
            var copyText = document.getElementById("exported-json");
          
            /* Select the text field */
            copyText.select();
            // copyText.setSelectionRange(0, 99999); /*For mobile devices*/
          
            /* Copy the text inside the text field */
            document.execCommand("copy");
          
            /* Alert the copied text */
            $('#exported-json').after(`
                <div id="copied-to-clipboard" class="alert alert-success text-center">
                    <i class="fa fa-check"></i> Data copied to clipboard.
                </div>
            `);

            $.when($(document).find('#copied-to-clipboard').fadeOut(5000)).done(function(){
                $(document).find('#copied-to-clipboard').remove();
            });
            // alert("Copied the text: " + copyText.value);
          });


        $(document).on('keyup', '.edit-element-control ', function(e){
            element_editor(this);
        });

        
        $(document).on('click', '#clear-form', function(e){
            let allForms = $(document).find('.form-element-container');
            
            allForms.addClass('bg-danger');

            if(confirm('Are you sure?'))
            {
                $.when(allForms.fadeOut('slow')).done(function(){
                    allForms.remove();
                    export_data();
                });
            }else{
                allForms.removeClass('bg-danger');
            }
        });

    });
})(jQuery)



function export_data()
{
    let 
        elements = $(document).find('.form-element-container'),
        singleJSON = {},
        data = [];

    // console.log($(elements[0]).data('json'));

    $.each(elements, function(i,v){
        let obj = $(v).data('json');
        obj.attributes = {};

        if(obj.hasOwnProperty('label'))
        {
            obj.labelText = obj.label;
            delete obj.idName;
        }
        if(obj.hasOwnProperty('idName'))
        {
            obj.forIdName = obj.idName;
            // obj.attributes.name = obj.idName;
            delete obj.idName;
        }
        if(obj.hasOwnProperty('className'))
        {
            obj.attributes.class = obj.className;
            delete obj.className;
        }
        if(obj.hasOwnProperty('placeholder'))
        {
            obj.attributes.placeholder = obj.placeholder;
            delete obj.placeholder;
        }
        if(obj.hasOwnProperty('autofocus'))
        {
            obj.attributes.autofocus = obj.autofocus;
            delete obj.autofocus;
        }
        if(obj.hasOwnProperty('readonly'))
        {
            obj.attributes.readonly = obj.readonly;
            delete obj.readonly;
        }
        if(obj.hasOwnProperty('disabled'))
        {
            obj.attributes.disabled = obj.disabled;
            delete obj.disabled;
        }
        data.push(obj);
    });

    $('.exported-json').val(JSON.stringify(data));
    // $('#POPUP').modal('show');
}



function element_editor(data)
{
    let 
        element_uid     = $(data).closest('form').data('el-uid'),
        data_target     = $(data).data('target'),
        this_pos        = data_target=='options' ? $(data).closest('.option-control').index() : -1,
        update_value    = $(data).val(),
        el              = $(document).find(`.form-element-container[data-uid=${element_uid}]`),
        current_json    = el.data('json'),
        update_json     = {};
    // console.log('forIdName', current_json);

    // console.log(element_uid, data_target, update_value);
    console.log(data_target, update_value);

    switch(data_target)
    {
        case 'label' : 
            current_json.label = update_value;
            el.find(`label[for=${element_uid}]`).text(update_value);
            break;

        case 'description' : 
            current_json.description = update_value;
            el.find(`.element-description`).text(update_value);
            break;

        case 'placeholder' : 
            current_json.placeholder = update_value;
            if(current_json.type=='select'){
                el.find(`[data-type="form-element"]>option:nth-child(1)`).text(update_value);
            }else{
                el.find(`[data-type="form-element"]`).attr('placeholder', update_value);
            }
            break;

        case 'idName' : 
            current_json.idName = update_value;
            current_json.forIdName = update_value;
            el.find(`[data-type="form-element"]`).attr('id', update_value);
            break;

        case 'className' : 
            current_json.className = update_value;
            el.find(`[data-type="form-element"]`).attr('class', update_value);
            break;
        
        case 'image-width' : 
            current_json.width = update_value;
            el.find(`[data-type="form-element"]`).attr('width', update_value+'px');
            break;
        
        case 'image-height' : 
            current_json.height = update_value;
            el.find(`[data-type="form-element"]`).attr('height', update_value+'px');
            break;
        
        case 'reportColName' : 
            current_json.reportColName = update_value;
            break;
        
        case 'col-rule' : 
            current_json['col-rule'] = update_value;
            break;
        
        case 'col-foot-rule' : 
            current_json['col-foot-rule'] = update_value;
            break;

        case 'options' : 
            // current_json.options[this_pos] = update_value;
            let option_obj = $(document).find('.edit-element-control.edit-options'),
                select_html = data.placeholder.length>0 ? `<option selected disabled>${current_json.placeholder}</option>` : '',
                checkbox_html = '',
                radio_html = '',
                options_array=[];
            $.each(option_obj, function(i,v){
                options_array.push($(v).val());
                select_html += `<option>${$(v).val()}</option>`;
                checkbox_html +=  `
                    <label class="d-block">
                        <input type="checkbox" name="${current_json.name}[]" value="${$(v).val()}" id="${NOW()}" class="${current_json.className}" data-type="form-element"> <span>${$(v).val()}</span>
                    </label>
                `;
                radio_html +=  `
                    <label class="d-block">
                        <input type="radio" name="${current_json.name}" value="${$(v).val()}" id="${NOW()}" class="${current_json.className}" data-type="form-element"> <span>${$(v).val()}</span>
                    </label>
                `;
            });
            current_json.options = options_array;


            if(current_json.type=='select')
            {
                el.find(`[data-type="form-element"]`).html(select_html);
            }
            else if(current_json.type=='check_boxes')
            {
                // el.find(`.checkboxes label:nth-child(${this_pos+1}) span`).text(update_value);
                el.find(`.checkboxes`).html(checkbox_html);
            }
            else if(current_json.type=='radio_buttons')
            {
                // el.find(`.radios label:nth-child(${this_pos+1}) span`).text(update_value);
                el.find(`.radios`).html(radio_html);
                // el.find(`.radios label:nth-child(${this_pos+1}) input`).val(update_value);
            }
            break;
        
    }
    // console.log(this_pos);
    export_data();

    // console.log(element_uid, data_target, el.data());

}





class FormElement{
    constructor(formData){
        try
        {
            // console.log('formData', formData);
            for (let key in formData) {
                this[key] = formData[key];
            }
            // console.log('THIS', this);

            let idName = formData.hasOwnProperty('idName') ? formData.idName : 'ID';
            this.uid = formData.uid = idName+NOW();
            this.label = formData.label = formData.hasOwnProperty('label') ? formData.label: 'Form Label';
            this.className = formData.className = formData.hasOwnProperty('className') ? formData.className: '';
            this.reportColName = formData.reportColName = formData.hasOwnProperty('reportColName') ? formData.reportColName: '';

            if(typeof this[formData.type]!=='function')
                throw({msg:`Element "${formData.type}" definition not found!\nPlease check console.`});

            let element = this[formData.type](formData);
            $('#formElementsField').append(element.elementHTML);
            $(document).find(`\#${element.uid}`).focus();
            let new_born_element = $(`.form-element-container[data-uid=${element.uid}]`);
            new ElementEditorPanel(element.uid);
            export_data();
        }
        catch(err)
        {
            console.log('ERROR!',err);
            alert(err.msg)
        }
    }


    h1()
    {
        try
        {
            let 
                uid             = this.uid,
                label           = this.label,
                alt_text        = this.hasOwnProperty('alt_text') ? this.alt_text : '',
                name            = this.hasOwnProperty('name') ? this.name : uid,
                className       = this.hasOwnProperty('className') ? this.className : '',
                height          = this.hasOwnProperty('height')&&Number(this.height)>0 ? `height="${this.height}px"` : '',
                width           = this.hasOwnProperty('width')&&Number(this.width)>0 ? `width="${this.width}px"` : '',
                defaultImage    = this.hasOwnProperty('defaultImage') ? this.defaultImage : '';
            
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <h1 id="${name}" class="${className} element-description" data-type="form-element">${label}</h1>
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }
    }



    heading_element()
    {
        try
        {
            let 
            uid             = this.uid,
            label           = this.label,
            description     = this.hasOwnProperty('description') ? this.description : '',
            name            = this.hasOwnProperty('name') ? this.name : uid,
            className       = this.hasOwnProperty('className') ? this.className : '',
            placeholder     = this.hasOwnProperty('placeholder') ? `placeholder="${this.placeholder}"` : '',
            default_value   = this.hasOwnProperty('default_value') ? this.default_value : '';
        
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <textarea name="${name}" id="${uid}" class="${className}" ${placeholder} data-type="form-element">${default_value}</textarea>
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log('ERROR',err);
            alert('ERROR! Please check console.')
        }

    }
   


    image()
    {
        try
        {
            let 
                uid             = this.uid,
                label           = this.label,
                name            = this.hasOwnProperty('name') ? this.name : uid,
                className       = this.hasOwnProperty('className') ? this.className : '',
                height          = this.hasOwnProperty('height')&&Number(this.height)>0 ? `height="${this.height}px"` : '',
                width           = this.hasOwnProperty('width')&&Number(this.width)>0 ? `width="${this.width}px"` : '',
                defaultImage    = this.hasOwnProperty('defaultImage') ? this.defaultImage : '';
            
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <div class="row">
                        <div class="col-12 text-center">
                            <img alt="${this.description}" src="${defaultImage}" id="${name}" class="d-inline ${className}" ${height} ${width} data-type="form-element" data-type="form-element">
                        </div>
                    </div>
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }
    }



    check_boxes()
    {
        try
        {
            let 
                uid         = this.uid,
                label       = this.label,
                description = this.hasOwnProperty('description') ? this.description : '',
                name        = this.hasOwnProperty('name') ? this.name : uid,
                className   = this.hasOwnProperty('className') ? this.className : '',
                placeholder = this.hasOwnProperty('placeholder') ? this.placeholder : '',
                options     = this.hasOwnProperty('options') ? this.options : [],
                optionsHTML = '',
                multiple    = this.hasOwnProperty('multiple')&&this.multiple ? `multiple` : '';

            options.forEach(function(v,i,arr){
                optionsHTML += `
                    <label class="d-block">
                        <input type="checkbox" name="${name}[]" value="${v}" id="${uid}${i>0?i:''}" class="${className}" data-type="form-element"> <span>${v}</span>
                    </label>
                `;
            })
            
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <div class="checkboxes">${optionsHTML}</div>
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }

    }
    


    radio_buttons()
    {
        try
        {
            let 
                uid         = this.uid,
                label       = this.label,
                description = this.hasOwnProperty('description') ? this.description : '',
                name        = this.hasOwnProperty('name') ? this.name : uid,
                className   = this.hasOwnProperty('className') ? this.className : '',
                placeholder = this.hasOwnProperty('placeholder') ? this.placeholder : '',
                options     = this.hasOwnProperty('options') ? this.options : [],
                optionsHTML = '',
                multiple    = this.hasOwnProperty('multiple')&&this.multiple ? `multiple` : '';

            options.forEach(function(v,i,arr){
                optionsHTML += `
                    <label class="d-block">
                        <input type="radio" name="${name}" value="${v}" id="${uid}${i>0?i:''}" class="${className}" data-type="form-element"> <span>${v}</span>
                    </label>
                `;
            })
            
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <div class="radios">${optionsHTML}</div>
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }

    }
    


    select()
    {
        try
        {
            let 
                uid         = this.uid,
                label       = this.label,
                description = this.hasOwnProperty('description') ? this.description : '',
                name        = this.hasOwnProperty('name') ? this.name : uid,
                className   = this.hasOwnProperty('className') ? this.className : '',
                placeholder = this.hasOwnProperty('placeholder') ? this.placeholder : '',
                options     = this.hasOwnProperty('options') ? this.options : [],
                optionsHTML = this.hasOwnProperty('placeholder') ? `<option selected disabled>${this.placeholder}</option>` : '',
                multiple    = this.hasOwnProperty('multiple')&&this.multiple ? `multiple` : '';

            options.forEach(function(v,i,arr){
                optionsHTML += `<option>${v}</option>`;
            })
            
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <select type="select" name="${name}" id="${uid}" class="${className}" ${multiple} data-type="form-element">${optionsHTML}</select>
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }

    }
    


    number()
    {
        try
        {
            let 
                uid         = this.uid,
                label       = this.label,
                description = this.hasOwnProperty('description') ? this.description : '',
                name        = this.hasOwnProperty('name') ? this.name : uid,
                className   = this.hasOwnProperty('className') ? this.className : '',
                placeholder = this.hasOwnProperty('placeholder') ? `placeholder="${this.placeholder}"` : '',
                min         = this.hasOwnProperty('min') ? `min="${this.min}"` : '',
                max         = this.hasOwnProperty('max') ? `max="${this.max}"` : '',
                step        = this.hasOwnProperty('step') ? `step="${this.step}"` : '';
            
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <input type="number" ${min} ${max} ${step} name="${name}" id="${uid}" class="${className}" ${placeholder} data-type="form-element">
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }
    }
 

    textarea()
    {
        try
        {
            let 
            uid             = this.uid,
            label           = this.label,
            description     = this.hasOwnProperty('description') ? this.description : '',
            name        = this.hasOwnProperty('name') ? this.name : uid,
            className       = this.hasOwnProperty('className') ? this.className : '',
            placeholder     = this.hasOwnProperty('placeholder') ? `placeholder="${this.placeholder}"` : '',
            default_value   = this.hasOwnProperty('default_value') ? this.default_value : '';
        
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <textarea name="${uid}" id="${name}" class="${className}" ${placeholder} data-type="form-element">${default_value}</textarea>
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log('ERROR',err);
            alert('ERROR! Please check console.')
        }

    }
     

    text()
    {
        try
        {
            let 
            uid         = this.uid,
            label       = this.label,
            description = this.hasOwnProperty('description') ? this.description : '',
            name        = this.hasOwnProperty('name') ? this.name : uid,
            className   = this.hasOwnProperty('className') ? this.className : '',
            placeholder = this.hasOwnProperty('placeholder') ? `placeholder="${this.placeholder}"` : '';
        
            this.elementHTML = `
                <div class="form-group form-element-container" data-uid="${uid}" data-json='${JSON.stringify(this)}'>
                    ${this.__titles()}
                    <input type="text" name="${name}" id="${uid}" class="${className}" ${placeholder} data-type="form-element">
                </div>
            `;
            return this;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }

    }
    

    __titles()
    {
        try
        {
            return `
                <div class="row">
                    <div class="col-1">
                        <i title="Move" class="fa fa-bars fa-lg handle"></i>
                    </div>
                    <div class="col-8">
                        <label for="${this.uid}" class="mb-0">${this.label}</label>
                        <div class="text-muted mb-1 element-description">${this.hasOwnProperty('description') ? this.description : ''}</div>
                    </div>
                    <div class="col-3">
                        ${this.__control_buttons()}
                    </div>
                </div>
            `;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }
    }

    __control_buttons()
    {
        try
        {
            return `
                <span class="float-right" data-uid="${this.uid}">
                    <button title="Edit" class="form-element-control-btn form-element-control-btn-edit"><i class="fas fa-pencil-alt fa-lg text-primary"></i></button>
                    <button title="Remove" class="form-element-control-btn form-element-control-btn-remove"><i class="fas fa-times fa-lg text-danger"></i></button>
                </span>
            `;
        }
        catch(err)
        {
            console.log(err);
            alert('ERROR! Please check console.')
        }
    }

};









class ElementEditorPanel 
{

    // hello = ['amin', 'hasan'];

    constructor(uid)
    {
        this.uid = uid;
        this.element_selector = $(document).find(`.form-element-container[data-uid=${this.uid}]`);
        this.element_data = this.element_selector.data('json');
        this.__make_all_elements_inactive();
        this.__make_element_active();
        this.__open_editor_plan();
        // console.log('this.element_data', this.element_data);
    }

    __open_editor_plan()
    {
        this.__editor_pan_loading_state();
        let html = this.__get_editing_fields();

        html = `<form data-el-uid="${this.uid}">${html}</form>`;

        setTimeout(function(){
            $('#editor-field').html(html);
            // new 
            if($(document).find('#EL_OPTIONS').length)
            {
                new Sortable(EL_OPTIONS, {
                    animation: 150,
                    handle: '.drag-handle', // handle's class
                    ghostClass: 'blue-background-class'
                });
            }
        
            activate_editor_submit_btn();
        }, 500);
    }

    __get_editing_fields()
    {
        let 
            ed      = this.element_data,
            es      = this.element_selector,
            el      = $(document).find(`\#${ed.uid}`),
            el_id   = el.attr('id'),
            el_name = el.attr('name') ? el.attr('name').replace('[','').replace(']','') : '',
            col_rule = ed.hasOwnProperty('col-rule') ? ed['col-rule'] : '',
            col_foot_rule = ed.hasOwnProperty('col-foot-rule') ? ed['col-foot-rule'] : '',
            html    = '';
        
        html += `
            <div class="form-group">
                <label for="edit-element-id">Element UID</label>
                <input id="edit-element-id" class="edit-element-control form-control" value="${ed.uid}" readonly>
            </div>
        `;

        html += `
            <div class="form-group">
                <label for="edit-label">Label</label>
                <input id="edit-label" class="edit-element-control form-control" value="${ed.label}" data-target="label" required>
            </div>
        `;

        
        if(ed.hasOwnProperty('tag_options'))
        {
            let 
            options_html = `<label>Tag</label> : `,
            selected;
            for (let key in ed.tag_options) 
            {
                selected = ed.type==ed.tag_options[key] ? 'checked' : '';
                options_html += `
                    <label for="edit-tag-option-${key}">
                        <input type="radio" name="${ed.uid}" id="edit-tag-option-${key}" class="edit-element-control edit-control checkbox" value="${ed.tag_options[key]}" data-target="tag_options" ${selected}> ${ed.tag_options[key]}
                    </label>&nbsp;
                `;
            }
            html += options_html;
            form-group
        }


        html += `
            <div class="form-group">
                <label for="edit-description">Description</label>
                <input id="edit-description" class="edit-element-control form-control" value="${ed.description}" data-target="description" required>
            </div>
        `;

        if(ed.type!='image')
        {
            html += `
                <div class="form-group">
                    <label for="edit-placeholder">Placeholder</label>
                    <input id="edit-placeholder" class="edit-element-control form-control" value="${ed.placeholder}" data-target="placeholder" required>
                </div>
            `;
        }

        if(ed.type=='image')
        {
            html += `
                <div class="form-group">
                    <label for="edit-placeholder">Placeholder</label>
                    <input type="file" id="edit-image" class="edit-element-control form-control" value="${ed.placeholder}" data-target="image-file" required>
                </div>
                <div class="form-group row">
                    <div class="col-6">
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span title="Drag to move order" class="input-group-text"><i class="fas fa-arrows-alt-h"></i></span>
                            </div>
                            <input type="number" id="img-width" class="edit-element-control edit-img-width form-control" value="${ed.width}" min="10" step="10" data-target="image-width">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span title="Drag to move order" class="input-group-text"><i class="fas fa-arrows-alt-v"></i></span>
                            </div>
                            <input type="number" id="img-height" class="edit-element-control edit-img-height form-control" value="${ed.height}" min="10" step="10" data-target="image-height">
                        </div>
                    </div>
                </div>
            `;
        }

        if(ed.hasOwnProperty('options'))
        {
            let options_html = '';
            for (let key in ed.options) 
            {
                options_html += `
                    <div id="${NOW()}_${key}" class="input-group mb-1 option-control">
                        <div class="input-group-prepend drag-handle">
                            <span title="Drag to move order" class="input-group-text"><i class="fa fa-bars"></i></span>
                        </div>
                        <input type="text" class="edit-element-control edit-options form-control" data-target="options" value="${ed.options[key]}">
                        <div class="input-group-append">
                            <button title="Remove this option" class="option-control-btn btn btn-outline-danger" type="button" data-optioncontrol="remove"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="input-group-append">
                            <button title="Add option before this" class="option-control-btn btn btn-outline-success p-1" type="button" data-optioncontrol="add-before"><i class="fa fa-arrow-up"></i> <i class="fa fa-plus"></i></button>
                        </div>
                        <div class="input-group-append">
                            <button title="Add option after this" class="option-control-btn btn btn-outline-success p-1" type="button" data-optioncontrol="add-after"><i class="fa fa-plus"></i> <i class="fa fa-arrow-down"></i></button>
                        </div>
                    </div>
                `;
            }
            html += `
                <div class="form-group">
                    <label>Options</label>
                    <div id="EL_OPTIONS">${options_html}</div>
                </div>
            `;

        }

        html += `<div class="" id="more-editor-options">`;

        html += `
            <div class="form-group">
                <label for="edit-reportColName">Report Column Name</label>
                <input id="edit-reportColName" class="edit-element-control form-control" value="${ed.reportColName}" data-target="reportColName" required>
            </div>
        `;
        

        html += `
            <div class="form-group">
                <label for="edit-idName">ID</label>
                <input id="edit-idName" class="edit-element-control form-control" value="${ed.idName}" data-target="idName" required>
            </div>
        `;
        

        html += `
            <div class="form-group">
                <label for="edit-className">Class</label>
                <input id="edit-className" class="edit-element-control form-control" value="${ed.className}" data-target="className" required>
            </div>
        `;

        html += `
            <div class="form-group">
                <label for="edit-name">Name</label>
                <input id="edit-name" class="edit-element-control form-control" value="${el_name}" data-target="name" required>
            </div>
        `;

        html += `
            <div class="form-group">
                <label for="edit-required">
                    <input type="checkbox" id="edit-required" class="edit-element-control checkbox" data-target="required" value="true"> Required
                </label>
                <label for="edit-disabled">
                    <input type="checkbox" id="edit-disabled" class="edit-element-control checkbox" data-target="disabled" value="true"> Disabled
                </label>
                <label for="edit-readonly">
                    <input type="checkbox" id="edit-readonly" class="edit-element-control checkbox" data-target="readonly" value="true"> Readonly
                </label>
            </div>
        `;

        html += `
            <div class="form-group">
                <label for="edit-col-rule">Column Rule</label>
                <input id="edit-col-rule" class="edit-element-control form-control" value="${col_foot_rule}" data-target="col-rule" required>
            </div>
        `;

        html += `
            <div class="form-group">
                <label for="edit-col-foot-rule">Column Foot Rule</label>
                <input id="edit-col-foot-rule" class="edit-element-control form-control" value="${col_rule}" data-target="col-foot-rule" required>
            </div>
        `;


        html += '</div>';

        return html;
    }

    __editor_pan_loading_state(){
        editor_pan_loading_state();
    }

    __make_all_elements_inactive(){
        make_all_elements_inactive();
    }

    __make_element_active(){
        this.element_selector.addClass('active');
    }

}






const NOW = function(){
    return new Date().getTime();
}



function reset_editor_pan()
{
    make_all_elements_inactive();
    disable_editor_submit_btn();
    editor_pan_loading_state();
    $('#reload-editor>i').addClass('fa-spin');
    setTimeout(function(){
        $('#reload-editor>i').removeClass('fa-spin');
        $('#editor-field').html(`
            <div class="alert alert-warning text-center mt-5">
                <p class="h3">Please select an input field to edit element options.</p>
            </div>
        `);
    }, 500);
}
function editor_pan_loading_state()
{
    $('#reload-editor>i').addClass('fa-spin');
    $('#editor-field').html(`
        <p class="text-muted text-center mt-5">
            <i class="fa fa-sync fa-4x fa-spin"></i>
        </p>
    `);
}
function make_all_elements_inactive(){
    $(document).find('.form-element-container.active').removeClass('active');
}
function disable_editor_submit_btn(){
    $('#submit-edit').removeClass('btn-success').addClass('btn-secondary').prop('disabled', true);
}
function activate_editor_submit_btn(){
    $('#reload-editor>i').removeClass('fa-spin');
    $('#submit-edit').removeClass('btn-secondary').addClass('btn-success').prop('disabled', false);
}






function reduild_editor(feedData)
{
    for(key in feedData)
    {
        let inputObj = feedData[key];
        if(inputObj.hasOwnProperty('attributes'))
        {
            if(inputObj.hasOwnProperty('label'))
            {
                inputObj.label = inputObj.attributes.labelText;
            }
            for(attrKey in inputObj.attributes)
            {
                console.log('inputObj.attributes', inputObj.attributes);

                if(inputObj.attributes.hasOwnProperty('forIdName'))
                {
                    inputObj.idName = inputObj.attributes.forIdName;
                    inputObj.name = inputObj.attributes.forIdName;
                }
                if(inputObj.attributes.hasOwnProperty('class'))
                {
                    inputObj.className = inputObj.attributes.class;
                }
                if(inputObj.attributes.hasOwnProperty('placeholder'))
                {
                    inputObj.placeholder = inputObj.attributes.placeholder;
                }
                if(inputObj.attributes.hasOwnProperty('autofocus'))
                {
                    inputObj.autofocus = inputObj.attributes.autofocus;
                }
                if(inputObj.attributes.hasOwnProperty('readonly'))
                {
                    inputObj.readonly = inputObj.attributes.readonly;
                }
                if(inputObj.attributes.hasOwnProperty('disabled'))
                {
                    inputObj.disabled = inputObj.attributes.disabled;
                }
                
            }
        }
        delete inputObj.attributes;
        // console.log(inputObj);
        new FormElement(inputObj);
    }
}





function import_data()
{

            
    try{
        let 
        existing_json = JSON.parse(String($('#existing-json').val())),
        elements = existing_json.hasOwnProperty('inputs') ? existing_json.inputs : [],
        singleJSON = {},
        data = [];
        console.log(elements);


        $.each(elements, function(i,obj){
            if(obj.hasOwnProperty('attributes'))
            {
                //     
                if(obj.attributes.hasOwnProperty('labelText'))
                {
                    obj.label = obj.attributes.labelText;
                }
                if(obj.hasOwnProperty('forIdName'))
                {
                    obj.idName = obj.forIdName;
                }else if(obj.attributes.hasOwnProperty('id'))
                {
                    obj.idName = obj.attributes.id;
                }
                if(obj.attributes.hasOwnProperty('class'))
                {
                    obj.className = obj.attributes.class;
                }
                if(obj.attributes.hasOwnProperty('placeholder'))
                {
                    obj.placeholder = obj.attributes.placeholder;
                }
                if(obj.attributes.hasOwnProperty('autofocus'))
                {
                    obj.autofocus = obj.attributes.autofocus;
                }
                if(obj.attributes.hasOwnProperty('readonly'))
                {
                    obj.readonly = obj.attributes.readonly;
                }
                if(obj.attributes.hasOwnProperty('disabled'))
                {
                    obj.disabled = obj.attributes.disabled;
                }
            }
            new FormElement(obj);
        });
    }catch(err){
        alert('No form data, please create new.');
    }
}
