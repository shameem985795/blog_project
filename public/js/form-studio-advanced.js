

const jexcelDataEntryTypes = {
    "text":"Text Entry",
    "number":"Numeric Entry",
};

const excelColumns = [
    {readOnly:true}
   ,{}
   ,{}
   ,{}
];

const excelData = [
    ['কলামের গ্রুপ'],
    ['কলামের নাম'],
    ['রিপোর্ট-এ কলামের শিরোনাম'],
    ['তথ্যের ধরণ'],
    ['সাধারণ মান'],
    ['পাদটীকার সূত্র']
];


const jexcelContextMenu = function(obj, x, y, e) {
    let mySlection = [];
    let rows = obj.getSelectedRows(true);
    let cols = obj.getSelectedColumns(true);
    let merges = obj.getMerge();
    let firstCellData = obj.getCell([rows[0], cols[0]], true);
    let firstCellName = jexcel.getColumnNameFromId([rows[0], cols[0]]);
    let items = [];

    // console.log('getMerge', mySpreadsheet.getMerge('B1'));
    // console.log('firstCellData', firstCellData);
    // console.log('hasMerge', hasMerge);

    if(y==null)
    {
        // Insert a new column Before
        if (obj.options.allowInsertColumn == true) {
            items.push({
                title:obj.options.text.insertANewColumnBefore,
                onclick:function() {
                    obj.insertColumn(1, parseInt(x), 1);
                }
            });
        }

        // Insert a new column After
        if (obj.options.allowInsertColumn == true) {
            items.push({
                title:obj.options.text.insertANewColumnAfter,
                onclick:function() {
                    obj.insertColumn(1, parseInt(x), 0);
                }
            });
        }

        // Delete a column
        if (obj.options.allowDeleteColumn == true) {
            items.push({
                title:obj.options.text.deleteSelectedColumns,
                onclick:function() {
                    obj.deleteColumn(obj.getSelectedColumns().length ? undefined : parseInt(x));
                }
            });
        }

        items.push({ type:'line' });
    }
    else 
    {

        // comments
        if (x && obj.options.allowComments == true) {

            var title = obj.records[y][x].getAttribute('title') || '';

            items.push({
                title: title ? obj.options.text.editComments : obj.options.text.addComments,
                onclick:function() {
                    obj.setComments([ x, y ], prompt(obj.options.text.comments, title)??title);
                }
            });

            if (title) {
                items.push({
                    title:obj.options.text.clearComments,
                    onclick:function() {
                        obj.setComments([ x, y ], '');
                    }
                });
            }
            items.push({ type:'line' });
        }

    }

    // Merge & Unmerge
    if (obj.options.merge) {
        items.push({
            title: obj.options.text.merge,
            onclick:function(){
                try
                {
                    if(rows.length>1 || rows[0]!=0)
                        throw('শুধুমাত্র \"কলামের গ্রুপ-\"এর ঘরসমূহই একীভূত করা যাবে');
                    if(cols[0]==0)
                        throw('\"A\" শিরোনামের ঘরসমূহ একীভূত করা সম্ভব নয়');

                    
                    obj.setMerge(mySlection.firstcell, mySlection.colspan, mySlection.rowspan);

                    mySlection.firstcell = [rows[0], cols[0]];

                    mySlection.colspan = rows.length;
                    mySlection.rowspan = cols.length;
                }
                catch(err)
                {
                    console.warn('ERROR!',err);
                    alert(`❌❌❌ দুঃখিত!\n`+err);
                }

            }
        });
        items.push({
            title: obj.options.text.unmerge,
            onclick:function(){
                let 
                hasMerge = mySpreadsheet.getMerge(firstCellName),
                allMerges = mySpreadsheet.getMerge();
                console.log(obj.destroyMerged());

            }
        });
    }
    
    // About
    if (obj.options.about) {
        items.push({
            title:obj.options.text.about,
            onclick:function() {
                let params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=0,height=0,left=-1000,top=-1000`;
                window.open('/form-studio/about', 'About', params);
            }
        });
    }
    return items;
};

const jexcelEvents = {
    onSelect    : function(instance, x1, y1, x2, y2, origin) {
        var cellName1 = jexcel.getColumnNameFromId([x1, y1]);
        var cellName2 = jexcel.getColumnNameFromId([x2, y2]);
        var isSingleCell = x1==x2 && y1==y2;
        // console.log('The selection from ' + cellName1 + ' to ' + cellName2 + '');
        // console.log(isSingleCell, y1);
        if(isSingleCell && y1==3 && x1!=0)
        {
            initialize_data_type_update_modal(x1, cellName1);
        }

    },
    beforeChange : function(instance, cell, x, y, value) {
        var cellName = jexcel.getColumnNameFromId([x,y]);
        console.log('The cell ' + cellName + ' will be changed');
    }
};


const jexcelConfig = {
    // url: 'http://rms.a2i.gov.bd/data/jexcel.json',
    data                : excelData,
    search              : false,
    filters             : false,
    pagination          : false,
    columns             : excelColumns,
    merge               : true,
    allowComments       : true,
    defaultColWidth     : 250,
    allowRenameColumn   : false,
    allowDeleteRow      : false,
    allowInsertRow      : false,

    // Event handlers
    onselection         : jexcelEvents.onSelect,
    onbeforechange      : jexcelEvents.beforeChange,

    contextMenu         : jexcelContextMenu,
    
    // ,footers: [['','','','Total','=SUMCOL(TABLE(), COLUMN())']]
    text                : {
        areYouSureToDeleteTheSelectedColumns : `❓❓❓\nআপনি কি কলাম মুছতে চান?`,
        cellAlreadyMerged : `❌❌❌\nঘরসমূহ ইতিমধ্যেই একীভূত আছে!`,
        deleteSelectedColumns : `<i class="mdi mdi-table-column-remove fa-lg"></i>&nbsp; কলাম মুছে ফেলুন`,
        insertANewColumnBefore : `<i class=" mdi mdi-table-column-plus-before fa-lg"></i>&nbsp; এটির আগে কলাম যোগ করুন`,
        insertANewColumnAfter : `</i><i class=" mdi mdi-table-column-plus-after fa-lg"></i>&nbsp; এটির পরে কলাম যোগ করুন`,
        merge : `<i class="mdi mdi-table-merge-cells fa-lg"></i>&nbsp; ঘর সমূহ একীভূত করুন`,
        unmerge : `<i class="mdi mdi-table fa-lg"></i>&nbsp; একীভুত ঘরমূহ অবমুক্ত করুন`,
        addComments : `<i class="mdi mdi-comment-plus-outline fa-lg"></i>&nbsp; মন্তব্য যোগ করুন`,
        editComments : `<i class="mdi mdi-comment-processing-outline fa-lg"></i>&nbsp; মন্তব্য সম্পাদনা করুন`,
        clearComments : `<i class="mdi mdi-comment-remove-outline fa-lg"></i>&nbsp; মন্তব্য মুছে ফেলুন`,
        about : `<i class="fas fa-question-circle fa-lg"></i>&nbsp; About`
    },
    about               : 'www.pondit.com'


};


(function($){
    $(document).ready(function(){
        $('#left-menu-toggle').trigger('click');
        $(document).find('.init-tooltip').tooltip();
        $('#submit-edit').prop('disabled', true);

        mySpreadsheet = jexcel(document.getElementById('spreadsheet'), jexcelConfig);

        $(document).on('click', '#jexcelEntryDataTypeSubmit', function(e){
            jexcelEntryDataTypeUpdate();
        });

    });
})(jQuery)


function jexcelEntryDataTypeUpdate()
{
    try
    {
        let 
        cellID      = $('#jexcelSelectedCellID').val(),
        entryType   = $('.jexcelEntryDataTypeRadio:checked').val();
        console.log('entryType',entryType, cellID);
        if(entryType==undefined)
            throw('তালিকা থেকে তথ্যের ধরণ নির্বাচন করুন।');
        mySpreadsheet.setValue(cellID, entryType);
        $('#jexcelEntryDataTypeModal').modal('hide');

    }
    catch(err)
    {
        console.warn('ERROR!',err);
        alert(`❌❌❌ দুঃখিত!\n`+err);
    }
}


function initialize_data_type_update_modal(x1, cellName)
{
    try
    {
        let 
        colTitle    = mySpreadsheet.getHeader(x1),
        colHead     = mySpreadsheet.getCell([x1, 1]).textContent,
        currentType = mySpreadsheet.getCell([x1, 3]).textContent,
        modal_body  = '',
        radio_buttons = '';

        if(!(colHead.length>0))
            colHead = prompt(`আপনাকে অবশ্যই কলামের নাম নির্ধারণ করতে হবে।\nএখানে কলামের নাম লিখুন : `,'') ?? '';
        if(colHead.length>0)
            mySpreadsheet.setValueFromCoords(x1,1, colHead);
        else
            throw('কলামের নাম নির্ধারণ করে আবার চেষ্টা করুন');
                
        modal_body += `
            <div class="form-group">
                <input type="text" value="${cellName}" id="jexcelSelectedCellID" class="form-control" placeholder="jExcel CellID" readonly>
            </div>
        `;
                
        radio_buttons += `
            <div class="form-group">
                <label for="">Select Entry Type</label>
        `;
        for(type in jexcelDataEntryTypes)
        {
            let checked = currentType==type ? 'checked' : '';
            radio_buttons += `
                <div class="custom-control custom-radio">
                    <input ${checked} type="radio" name="jexcelEntryDataTypeRadio" value="${type}" id="jexcelEntryDataType-${type}" class="custom-control-input jexcelEntryDataTypeRadio">
                    <label class="custom-control-label" for="jexcelEntryDataType-${type}">${jexcelDataEntryTypes[type]}</label>
                </div>
            `;
        }
        radio_buttons += `</div>`;
        modal_body += radio_buttons;

        $('#jexcelEntryDataTypeModal .modal-body').html(modal_body);
        $('#jexcelEntryDataTypeSelectedCellName').text(colHead.length>0?colHead:colTitle);
        $('#jexcelEntryDataTypeModal').modal('show');

    }
    catch(err)
    {
        console.log(err);
        // mySpreadsheet.resetSelection();
        mySpreadsheet.updateSelectionFromCoords(x1,1,x1,1);
        console.warn('ERROR!',err);
        alert(`❌❌❌ দুঃখিত!\n`+err);
    }
}